# Quantum Integrated Development Environment

![sc](/runshots/qpm.png)

## Features

Unlease the power of OpenQL and QX-Sim for large-scale Quantum Algorithm Development

* Code in OpenQL
    * Python editor with syntax highlighting
    * 1-click code convert to QASM 2.0
    * Predictive Kernel framework insert (TBD)
* Code in QASM 2.0
    * 1-click circuit generation
    * 1-click execution and result plotting
* Code in QCirc 2.0
    * Intuitive and easy development
    * QX-Sim full width support of 50 qubits
    * Save circuit as high-resolution images
    * Select Gate Set Toolbox based on underlying hardware
    * Define custom gate bundle (kernels) by dragging box over circuit elements and reuse them (TBD)
    * Define gates with unitary matrix with auto-decomposition (TBD)
    * Convert code back to QASM 2.0 (TBD)
    * Define circuit run repeatations for statistical results (TBD)

## Other Features

* Apache 2 License
* Cross-platform design in PyQt4
* Layout based QCirc designing (TBD)
* Under active development for Alpha release: suggestions are welcome